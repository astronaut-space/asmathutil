//
//  ViewController.swift
//  ASMathUtil
//
//  Created by astronaut on 01/11/2022.
//  Copyright (c) 2022 astronaut. All rights reserved.
//

import UIKit
import ASMathUtil

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let angle = Angle(36)
        print("测试引入ASMathUtil, 角度为:", angle.value)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

