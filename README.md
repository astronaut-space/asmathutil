# ASMathUtil

[![CI Status](https://img.shields.io/travis/astronaut/ASMathUtil.svg?style=flat)](https://travis-ci.org/astronaut/ASMathUtil)
[![Version](https://img.shields.io/cocoapods/v/ASMathUtil.svg?style=flat)](https://cocoapods.org/pods/ASMathUtil)
[![License](https://img.shields.io/cocoapods/l/ASMathUtil.svg?style=flat)](https://cocoapods.org/pods/ASMathUtil)
[![Platform](https://img.shields.io/cocoapods/p/ASMathUtil.svg?style=flat)](https://cocoapods.org/pods/ASMathUtil)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ASMathUtil is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ASMathUtil'
```

## Author

astronaut, z13976100@163.com

## License

ASMathUtil is available under the MIT license. See the LICENSE file for more info.
